// we need the include files io and stdlib and string and stdbool and time
// we need to define a constant for the number of rooms
// we need to define a constant for the maximum number of connections per room
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// we want to write a text based game that is a little bit like a choose your own adventure book
// each turn we will present the player with a choice of actions
// the player will choose an action and we will carry it out
// the player can look at the room, or move to another room

// we will need to keep track of the current room
// we need a way to represent the rooms
// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the rooms
// room has connections and connections have rooms so we need to define connection first but we can't because room needs it
// we can use a forward declaration to tell the compiler that connection is a struct that will be defined later
typedef struct connection connection;

typedef struct npc {
    char* name;
    char* description;
    int goldBars; // New attribute to store gold bars
    bool sellsItem;
    char* itemName;
    int itemPrice;
    bool isMarketSeller; // This field signifies if the NPC is the market seller
    // Add other NPC attributes as needed
} npc;

typedef struct room {
  char* name;
  char* description;
  connection* connections;
  int numConnections;
  npc* npcs; // Array of NPCs in the roomint numNPCs;
  int numNPCs;
} room;

// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the connections
typedef struct connection {
  room* room1;
  room* room2;
} connection;

// we need a way to represent the player
// let's have a datastructure that represents the player
typedef struct player {
  room* currentRoom;
  int goldBars; // New field to track gold
  char* inventory[10];
  int inventorySize;
  bool hasKey; // Flag to track if the player has the key
} player;

// we need a way to represent the game
// let's have a datastructure that represents the game
typedef struct game {
  room* rooms;
  int numRooms;
  player* player;
} game;

// Helper function to find the index of a room by name
int findRoomIndexByName(room* rooms, int numRooms, const char* name) {
  for (int i = 0; i < 10; ++i) {
    if (strcmp(rooms[i].name, name) == 0) {
        return i;
        }
    }
    return -1; // Room not found
}

// Function to display the player's inventory
void displayInventory(player* currentPlayer) {
    printf("\033[0;32m"); // Set color to green (change the color code as needed)
    printf("Inventory:\n");
    printf("\033[0m"); // Reset color to default
    if (currentPlayer->inventorySize == 0) {
        printf("\033[0;31m"); // Set color to red
        printf("Empty\n");
        printf("\033[0m"); // Reset color to default
    } else {
        for (int i = 0; i < currentPlayer->inventorySize; i++) {
            printf("%d. %s\n", i + 1, currentPlayer->inventory[i]);
        }
    }
}

// let's have a function to print a menu and get user choice, return the choice
int getMenuChoice(player* currentPlayer, game* gameInstance, room* rooms, int numRooms, bool hasKey) {
    int choice;
    printf("\033[0;33m"); // Set color to yellow
    printf("Gold held: %d\n", currentPlayer->goldBars); // Display current gold;
    printf("\033[0m"); // Reset color to default
    displayInventory(currentPlayer); // Display inventory
    printf("What would you like to do?\n");
    printf("1. Look around\n");
    printf("2. Move to another room\n");
    printf("3. Talk to NPC\n");
    printf("4. Quit\n"); // Renumbered to include the new option

    // Check if the player is in the Haunted Manor and add the key option if true
    FILE* roomsFile = fopen("rooms.csv", "r");
    if (roomsFile == NULL) {
        printf("Error opening rooms file\n");
        return -1;
    }

    char line[500];
    int hauntedManorRoomIndex = -1;
    const char* hauntedManorName = "Haunted Manor";

    // Loop through each line in the rooms file
    char* trimmedName;
    while (fgets(line, sizeof(line), roomsFile) != NULL) {
        char* name = strtok(line, ",");
        if (name != NULL) {
            trimmedName = malloc(strlen(name) + 1);
            int len = strlen(name);
            strncpy(trimmedName, name + 1, len - 2); // Remove quotes from the name
            trimmedName[len - 2] = '\0'; // Null-terminate the string
        } else {
            printf("Error reading room name\n");
            break;
        }

        char* description = strtok(NULL, "\n");

        // Check if the current room matches the Haunted Manor
        if (strcmp(trimmedName, hauntedManorName) == 0) {
            hauntedManorRoomIndex = findRoomIndexByName(rooms, numRooms, hauntedManorName);
            break;
        }
        free(trimmedName);
    }

    fclose(roomsFile);

    if (hauntedManorRoomIndex != -1) {
      if (strcmp(gameInstance->player->currentRoom->name, hauntedManorName) == 0) {
        if (hasKey == true) {
        printf("5. Use the key to open the gate\n");
        } else {
        printf("5. Attempt to open the gate\n");
        }
      }
    }
    
    scanf("%d", &choice);

    // Check for valid choices
    while (choice < 1 || choice > 5) { // Adjust the upper limit based on the total options
        printf("Invalid choice, please try again\n");
        scanf("%d", &choice);
    }
    return choice;
}

// let's have a function to print the room description
void printRoomDescription(room* room) {
    printf("\033[0;36m"); // Set color to cyan
    printf("You are in the "); // Print the "You are in the" in cyan
    printf("\033[0;34m%s.\n", room->name); // Set color to blue for the room name
    printf("\033[0m"); // Reset color to default
    printf("\033[0;35m"); // Set color to magenta
    printf("%s\n", room->description); // Print the description
    printf("\033[0m"); // Reset color to default
}

// a function to get user choice of room to move to
int getRoomChoice(room* currentRoom) {
  int choice;
  
  while (1) {
    printf("Which room would you like to move to?\n");
    
    for (int i = 0; i < currentRoom->numConnections; i++) {
      printf("%d. %s\n", i + 1, currentRoom->connections[i].room2->name);
    }
    
    if (scanf("%d", &choice) != 1 || choice < 1 || choice > currentRoom->numConnections) {
      // Clear the input buffer
      int c;
      while ((c = getchar()) != '\n' && c != EOF);

      printf("Invalid choice, please try again\n");
    } else {
      break;
    }
  }

  return choice;
}
// a function to move the player to another room, and describe it to the user
void movePlayer(player* player, int choice) {
  player->currentRoom = player->currentRoom->connections[choice-1].room2;
  printRoomDescription(player->currentRoom);
}

void stealGold(npc* targetNPC, player* currentPlayer, bool* playerHasStolen) {
    // Implement the logic for stealing gold (50% chance)
    if (rand() % 2 == 0) {
        *playerHasStolen = true;
        printf("\033[0;31m"); // Set color to red
        printf("You successfully stole ");
        printf("\033[0;33m"); // Set color to yellow
        printf("%d gold bars ", targetNPC->goldBars); // Yellow text for the gold amount
        printf("\033[0;34m"); // Set color to blue
        printf("from %s!\n", targetNPC->name); // Blue text for the NPC name
        printf("\033[0m"); // Reset color to default
        currentPlayer->goldBars += targetNPC->goldBars; // Update player's gold after successful theft
        targetNPC->goldBars = 0; // Set NPC's gold to 0 after being stolen from
    } else {
        printf("\033[0;31m"); // Set color to red
        printf("Your attempt to steal from "); // Red text for the beginning of the sentence
        printf("\033[0;34m"); // Set color to blue
        printf("%s", targetNPC->name); // Blue text for the NPC name
        printf("\033[0;31m"); // Set color back to red for the end of the sentence
        printf(" failed!\n");
        printf("\033[0m"); // Reset color to default
    }
}

void askForGold(npc* targetNPC, player* currentPlayer, bool* playerGotGold) {
    // Implement the logic for asking for gold (70% chance to get 20% of NPC's gold)
    if (rand() % 10 < 7) {
        int goldGiven = targetNPC->goldBars * 0.2;
        *playerGotGold = true;
        printf("\033[0;36m"); // Set color to cyan
        printf("%s", targetNPC->name); // Blue text for the NPC name

        printf("\033[0;33m"); // Set color to yellow
        printf(" gave you ");

        printf("\033[0;33m"); // Set color to yellow
        printf("%d gold bars!\n", goldGiven);
        printf("\033[0m"); // Reset color to default
        currentPlayer->goldBars += goldGiven; // Update player's gold after receiving gold
        targetNPC->goldBars -= goldGiven; // Reduce NPC's gold after giving some to player
    } else {
      printf("\033[0;36m"); // Set color to cyan
      printf("%s", targetNPC->name); // Blue text for the NPC name
      printf("\033[0;36m"); // Set color to cyan
      printf(" refused to give you any gold!\n");
      printf("\033[0m"); // Reset color to default
    }

}
// a function to load the rooms from a file
// the file is called rooms.csv, and has a room name and room description on each line
// the function returns an array of rooms
room* loadRooms() {
  // open the file
  FILE* file = fopen("rooms.csv", "r");
  // we need to check that the file opened successfully
  if (file == NULL) {
    printf("Error opening file\n");
    exit(1);
  }
  // we need to count the number of lines in the file
  int numLines = 0;
  char line[500];
  while (fgets(line, 500, file) != NULL) {
    numLines++;
  }
  // we need to rewind the file
  rewind(file);
  // we need to allocate memory for the rooms
  room* rooms = malloc(sizeof(room) * numLines);
  // we need to read the rooms from the file
  for (int i = 0; i < numLines; i++) {
    // we need to read the line
    fgets(line, 500, file);
    // we need to remove the newline character
    line[strlen(line) - 1] = '\0';
    // we need to split the line into the name and description
    // the strings are in quotation marks, and the two quoted strings are separated by a comma
    // we need to split the line on ", " (the four characters)
    // everything between the first and second " is the name
    // everything between the third and fourth " is the description
    // we need to find the first "
    char* name = strtok(line, "\"");
    // we need to find the second " and record where it is so we can null terminate the string
    char* endofname = strtok(NULL, "\"");

    // we need to find the third "
    char* description = strtok(NULL, "\"");
    // we need to find the fourth "
    char* endofdesc = strtok(NULL, "\0");
    // we need to be sure that name and description are null-terminated strings
    name[endofname - name] = '\0';
    // description[endofdesc-description]='\0';
    // we need to create a room
    room room;
    // we need to copy the string into the room name
    room.name = malloc(sizeof(char) * strlen(name) + 1);
    strcpy(room.name, name);
    // we need to copy the string into the room description
    room.description = malloc(sizeof(char) * strlen(description) + 1);
    strcpy(room.description, description);
    room.connections = NULL;
    room.numConnections = 0;
    // Initialize the npcs array to NULL initially
    room.npcs = NULL;
    room.numNPCs = 0;
    // we need to add the room to the array
    rooms[i] = room;
  }
  // we need to close the file
  fclose(file);
  // we need to create a maze like set of connections between the rooms
  // we need to read the connections from the connections.csv file
  FILE* connectionsFile = fopen("connections.csv", "r");
  if (connectionsFile == NULL) {
    printf("Error opening connections file\n");
    exit(1);
  }

  char connectionLine[500];
  int room1, room2, weight;

  while (fgets(connectionLine, 500, connectionsFile) != NULL) {
    sscanf(connectionLine, "%d,%d,%d", &room1, &room2, &weight);

    // we need to create a connection between the rooms
    connection newConnection;
    newConnection.room1 = &rooms[room1];
    newConnection.room2 = &rooms[room2];

    // we need to add the connection to the room
    rooms[room1].connections = realloc(rooms[room1].connections, sizeof(connection) * (rooms[room1].numConnections + 1));
    rooms[room1].connections[rooms[room1].numConnections] = newConnection;
    rooms[room1].numConnections++;

    // add the connection in both directions
    newConnection.room1 = &rooms[room2];
    newConnection.room2 = &rooms[room1];

    rooms[room2].connections = realloc(rooms[room2].connections, sizeof(connection) * (rooms[room2].numConnections + 1));
    rooms[room2].connections[rooms[room2].numConnections] = newConnection;
    rooms[room2].numConnections++;
  }

  // close the connections file
  fclose(connectionsFile);

  // we need to return the array of rooms
  return rooms;
}

// Function to load NPCs

// Define a constant for the total number of unique NPCs
#define TOTAL_UNIQUE_NPCS 20
void loadNPCs(room* rooms, int numRooms) {
  const char* npcNames[TOTAL_UNIQUE_NPCS] = {
    "Guide", "Guardian", "Knight", "Wizard", "Merchant", "Thief", "Blacksmith", "Sorcerer", "Druid", "Rogue",
    "Warrior", "Cleric", "Bard", "Assassin", "Paladin", "Necromancer", "Ranger", "Barbarian", "Monk", "Alchemist"
  };

  const char* npcDescriptions[TOTAL_UNIQUE_NPCS] = {
    "I am the guide here, ready to offer you information about this room.",
    "I am the guardian of this place, entrusted with protecting its secrets.",
    "I am a noble knight, sworn to defend the realm from evil.",
    "I am a powerful wizard, capable of wielding the forces of magic.",
    "I am a merchant, selling wares from distant lands.",
    "I am a stealthy thief, always seeking valuable treasures.",
    "I am a skilled blacksmith, crafting weapons of great strength.",
    "I am a sorcerer, delving into ancient spells and mysteries.",
    "I am a druid, connected to the forces of nature.",
    "I am a rogue, navigating the shadows and secrets of cities.",
    "I am a fierce warrior, honing my skills in battle.",
    "I am a devoted cleric, serving the divine.",
    "I am a bard, spinning tales and songs of heroes.",
    "I am an assassin, unseen and deadly.",
    "I am a noble paladin, upholding justice and righteousness.",
    "I am a necromancer, mastering the dark arts.",
    "I am a ranger, attuned to the ways of the wild.",
    "I am a barbarian, embracing primal strength and fury.",
    "I am a monk, disciplined in mind and body.",
    "I am an alchemist, experimenting with concoctions and elixirs."
  };

  int npcIndex = 0; // Define npcIndex

  // Goblin Market NPC selling a key
  FILE* roomsFile = fopen("rooms.csv", "r");
  if (roomsFile == NULL) {
    printf("Error opening rooms file\n");
    return;
}

  char line[500];
  int goblinMarketRoomIndex = -1;
  const char* goblinMarketName = "Goblin Market";

  // Loop through each line in the rooms file
  char* trimmedName;
  while (fgets(line, sizeof(line), roomsFile) != NULL) {
    char* name = strtok(line, ",");
    if (name != NULL) {
        trimmedName = malloc(strlen(name) + 1);
        int len = strlen(name);
        strncpy(trimmedName, name + 1, len - 2); // Remove quotes from the name
        trimmedName[len - 2] = '\0'; // Null-terminate the string
    } else {
        printf("Error reading room name\n");
        break;
    }

    char* description = strtok(NULL, "\n");

    // Check if the current room matches the Goblin Market
    if (strcmp(trimmedName, goblinMarketName) == 0) {
        goblinMarketRoomIndex = findRoomIndexByName(rooms, numRooms, goblinMarketName);
        break;
    }
    free(trimmedName);
    }

  fclose(roomsFile);

    if (goblinMarketRoomIndex != -1) {
    rooms[goblinMarketRoomIndex].numNPCs = 1; // Set only one NPC for the Goblin Market
    rooms[goblinMarketRoomIndex].npcs = malloc(sizeof(npc) * rooms[goblinMarketRoomIndex].numNPCs);

    rooms[goblinMarketRoomIndex].npcs[0].name = malloc(strlen("Goblin Merchant") + 1);
    strcpy(rooms[goblinMarketRoomIndex].npcs[0].name, "Goblin Merchant");

    rooms[goblinMarketRoomIndex].npcs[0].description = malloc(strlen("A shrewd goblin merchant with various goods on display.") + 1);
    strcpy(rooms[goblinMarketRoomIndex].npcs[0].description, "A shrewd goblin merchant with various goods on display.");

    rooms[goblinMarketRoomIndex].npcs[0].goldBars = 0;
    rooms[goblinMarketRoomIndex].npcs[0].sellsItem = true;

    rooms[goblinMarketRoomIndex].npcs[0].itemName = malloc(strlen("Key") + 1);
    strcpy(rooms[goblinMarketRoomIndex].npcs[0].itemName, "Key");

    rooms[goblinMarketRoomIndex].npcs[0].itemPrice = 200;
    rooms[goblinMarketRoomIndex].npcs[0].isMarketSeller = true; // Set the Goblin Market NPC as the seller
    } else {
    printf("Goblin Market room not found in the CSV file.\n");
    }

    // Remove any additional NPCs from the Goblin Market room
    for (int i = 0; i < 10; ++i) {
      if (i != goblinMarketRoomIndex) {
       // Clear NPCs from all other rooms
       rooms[i].numNPCs = 0;
       free(rooms[i].npcs); // Free memory allocated for NPCs
       }
    }
      // Remove any additional NPCs from the Goblin Market room
for (int i = 0; i < numRooms; ++i) {
    if (i != goblinMarketRoomIndex) {
        // Clear NPCs from all other rooms
        rooms[i].numNPCs = 0;
        free(rooms[i].npcs); // Free memory allocated for NPCs
    }
}

// Assign NPCs for other rooms
for (int i = 0; i < 10; ++i) {
    if (i != goblinMarketRoomIndex) {
        int npcCount = rand() % 3 + 1; // Random value between 1 and 3 for NPCs in a room
        rooms[i].numNPCs = npcCount; // Set the number of NPCs for this room

        rooms[i].npcs = malloc(sizeof(npc) * npcCount);

        for (int j = 0; j < npcCount; ++j) {
            if (npcIndex >= TOTAL_UNIQUE_NPCS) {
                printf("Exceeded total unique NPCs.\n");
                return;
            }

            rooms[i].npcs[j].name = malloc(strlen(npcNames[npcIndex]) + 1);
            strcpy(rooms[i].npcs[j].name, npcNames[npcIndex]);

            rooms[i].npcs[j].description = malloc(strlen(npcDescriptions[npcIndex]) + 1);
            strcpy(rooms[i].npcs[j].description, npcDescriptions[npcIndex]);

            int randomGold = rand() % 41 + 10; // Random value between 10 and 50
            rooms[i].npcs[j].goldBars = randomGold;

            npcIndex++;
        }
    }
}
}
void interactWithNPC(room* currentRoom, game* gameInstance) {
    if (currentRoom->numNPCs == 0) {
        printf("No NPCs available in this room.\n");
        return;
    }

    printf("Available NPCs:\n");
    for (int i = 0; i < currentRoom->numNPCs; i++) {
        printf("%d. %s\n", i + 1, currentRoom->npcs[i].name);
    }

    int npcChoice;
    printf("Which NPC would you like to talk to?\n");
    scanf("%d", &npcChoice);

    // Ensure the chosen NPC is within the valid range
    if (npcChoice < 1 || npcChoice > currentRoom->numNPCs) {
        printf("Invalid NPC choice.\n");
        return;
    }
    printf("\033[0;36m"); // Set color to cyan
    printf("You engage in conversation with "); // Cyan text
    printf("\033[0;34m"); // Set color to blue
    printf("%s:\n", currentRoom->npcs[npcChoice - 1].name); // Blue text for NPC name
    printf("\033[0;32m"); // Set color to green
    printf("%s\n", currentRoom->npcs[npcChoice - 1].description); // green text for description
    printf("\033[0m"); // Reset color to default

    int interactionChoice;
    printf("1. Try to "); // Default color
    printf("\033[0;31m"); // Set color to red
    printf("steal"); // Red text for the word "steal"
    printf("\033[0m"); // Reset color to default
    printf(" \033[0;33mgold\033[0m\n"); // Yellow text for the word "gold"
    printf("\033[0m"); // Reset color to default
    printf("2. "); // Default color
    printf("\033[0;36m"); // Set color to cyan
    printf("Ask"); // Cyan text for the word "Ask"
    printf("\033[0m"); // Reset color to default
    printf(" for "); // Default color
    printf("\033[0;33m"); // Set color to yellow
    printf("gold\033[0m\n"); // Yellow text for the word "gold"
    printf("\033[0m"); // Reset color to default

    // Check if the NPC is the Goblin Merchant
    if (strcmp(currentRoom->npcs[npcChoice - 1].name, "Goblin Merchant") == 0) {
      printf("\033[0m"); // Reset color to default
      printf("3. "); // Default color
      printf("\033[0;32m"); // Set color to green
      printf("Trade"); // Green text for the word "Trade"
      printf("\033[0m"); // Reset color to default
      printf(" with the "); // Default color
      printf("\033[0;34m"); // Set color to blue
      printf("Goblin Merchant\033[0m\n"); // Blue text for the NPC
    }

    scanf("%d", &interactionChoice);

    bool playerHasStolen = false;
    bool playerGotGold = false;

    switch (interactionChoice) {
        case 1:
            stealGold(&currentRoom->npcs[npcChoice - 1], gameInstance->player, &playerHasStolen);
            break;
        case 2:
            askForGold(&currentRoom->npcs[npcChoice - 1], gameInstance->player, &playerGotGold);
            break;
        case 3:
            // Check if the player has the key
            bool playerHasKey = gameInstance->player->hasKey;
            if (strcmp(currentRoom->npcs[npcChoice - 1].name, "Goblin Merchant") == 0) {
              if (strcmp(currentRoom->name, "Goblin Market") == 0) {
                printf("\033[0;32m"); // Set color to green
                printf("Available items for sale:\n");
                printf("\033[0m"); // Reset color to default
                for (int i = 0; i < currentRoom->numNPCs; ++i) {
                  if (currentRoom->npcs[i].isMarketSeller) {
                    // Print the item for sale if the player doesn't have the key
                    if (!(playerHasKey && strstr(currentRoom->npcs[i].itemName, "Key") != NULL)) {
                      printf("\033[0m"); // Reset color to default
                      printf("%d. %s - Price: ", i + 1, currentRoom->npcs[i].itemName); // Default color
                      printf("\033[0;33m"); // Set color to yellow
                      printf("%d", currentRoom->npcs[i].itemPrice); // Yellow text for the amount
                      printf(" gold bars\n");
                      printf("\033[0m"); // Reset color to default
                                        }
                  }
                }
                
                int purchaseChoice;
                printf("\033[0m"); // Reset color to default
                printf("Enter the number of the "); // Default color
                printf("\033[0;35m"); // Set color to magenta
                printf("item"); // Magenta text for "item"
                printf("\033[0m"); // Reset color to default
                printf(" you want to "); // Default color
                printf("\033[0;32m"); // Set color to green
                printf("buy"); // Green text for "buy"
                printf("\033[0m"); // Reset color to default
                printf(": ");
                scanf("%d", &purchaseChoice);
            
            if (purchaseChoice >= 1 && purchaseChoice <= currentRoom->numNPCs) {
              int sellerIndex = purchaseChoice - 1;
              if (currentRoom->npcs[sellerIndex].isMarketSeller && gameInstance->player->goldBars >= currentRoom->npcs[sellerIndex].itemPrice) {
                printf("\033[0;32m"); // Set color to green
                printf("You "); // Green text for "You"
                printf("\033[0m"); // Reset color to default
                printf("bought the "); // Default color
                printf("\033[0;35m"); // Set color to magenta
                printf("%s", currentRoom->npcs[sellerIndex].itemName); // Magenta text for the item name
                printf("\033[0m"); // Reset color to default
                printf("!\n"); // Default color
                gameInstance->player->goldBars -= currentRoom->npcs[sellerIndex].itemPrice;
                // Add the key to the player's inventory if there's space
                if (gameInstance->player->inventorySize < 10) {
                  gameInstance->player->inventory[gameInstance->player->inventorySize] = malloc(strlen(currentRoom->npcs[sellerIndex].itemName) + 1);
                  strcpy(gameInstance->player->inventory[gameInstance->player->inventorySize], currentRoom->npcs[sellerIndex].itemName);
                  gameInstance->player->inventorySize++;
                  // Check if the purchased item is the key and set the hasKey flag
                  if (strstr(currentRoom->npcs[sellerIndex].itemName, "Key") != NULL) {
                    gameInstance->player->hasKey = true;
                  }
                } else {
                  printf("\033[0;31m"); // Set color to red
                  printf("Your inventory is full, cannot add the key.\n");
                  // Handle the case where the inventory is full
                }
              } else {
                printf("You don't have enough gold to buy that or it's not available.\n");
              }
            } else {
              printf("Invalid choice.\n");
            }
          } else {
            printf("This isn't the Goblin Market!\n");
          }
        } else {
          printf("Invalid choice.\n");
        }
        break;
    default:
        printf("Invalid choice.\n");
        printf("\033[0m"); // Reset color to default
        break;
      case 4: // Key interaction option
      if (strcmp(currentRoom->name, "Haunted Manor") == 0) {
        if (gameInstance->player->hasKey) {
            // Implement the action for using the key in Haunter Manor
            printf("You use the key and unlock a secret room!\n");
            // Implement the specific action here (e.g., reveal hidden items, trigger a special event, etc.)
        } else {
            printf("You need the key to unlock something here.\n");
        }
    } else {
        printf("There's nothing to unlock here.\n");
    }
    break;
    }
}

// let's have a function to create a player
player* createPlayer(room* currentRoom) {
  // we need to allocate memory for the player
  player* newPlayer = malloc(sizeof(player));
  // we need to set the current room
  newPlayer->currentRoom = currentRoom;
  // we need to return the player
  newPlayer->goldBars = 0; // Initialize player's gold to 0
  newPlayer->inventorySize = 0; // Initialize inventory size to zero
  newPlayer->hasKey = false; // Initialize the flag to false
  return newPlayer;
}

// let's have a function to create a game

game* createGame() {
  // we need to allocate memory for the game
  game* game = malloc(sizeof(game));
  // we need to load the rooms
  game->rooms = loadRooms();
  // we need to load the npcs
  loadNPCs(game->rooms, game->numRooms);
  // we need to set the number of rooms
  game->numRooms = 10;
  // we need to create the player
  game->player = createPlayer(&game->rooms[0]);
  // we need to return the game
  return game;
}

// let's have a function to play the game which is the main function
void playGame() {
    // Welcome message and game initialization
    printf("\033[0;36m"); // Set color to cyan
    printf("Welcome to the game\n");
    printf("\033[0m"); // Reset color to default
    game* gameInstance = createGame();
    
    // Display initial room description
    printRoomDescription(gameInstance->player->currentRoom);
    
    // Game loop until the user quits
    bool quit = false;
    while (!quit) {
        // Display menu and get user choice
        int choice = getMenuChoice(gameInstance->player, gameInstance, gameInstance->rooms, gameInstance->numRooms, gameInstance->player->hasKey);
        
        // Carry out user choice
        if (choice == 1) {
            // Print the room description
            printRoomDescription(gameInstance->player->currentRoom);
        } else if (choice == 2) {
            // Get user choice of room to move to
            int roomChoice = getRoomChoice(gameInstance->player->currentRoom);
            // Move the player to the room
            movePlayer(gameInstance->player, roomChoice);
        } else if (choice == 3) {
            // Talk to NPC
            interactWithNPC(gameInstance->player->currentRoom, gameInstance);
        } else if (choice == 4) {
            // Quit
            quit = true;
        } else if (choice == 5 && strcmp(gameInstance->player->currentRoom->name, "Haunted Manor") == 0) {
            if (gameInstance->player->hasKey) {
                // Player has the key and chooses to unlock the gate
                printf("\033[0;32m"); // Set color to green
                printf("You use the key to unlock the gate! Spooky noises fade away as the gate creaks open. You've escaped the simulation.\n");
                printf("\033[0m"); // Reset color to default
                quit = true; // Game ends after escaping
            } else {
                // Player doesn't have the key and chooses to attempt opening the gate
                printf("\033[0;31m"); // Set color to red
                printf("The gate won't move. You need a key to unlock it.\n");
                printf("\033[0m"); // Reset color to default
            }
        } else {
            printf("Invalid choice.\n");
        }
    }
}
// let's have a main function to call the playGame function
int main() {
  playGame();
  return 0;
}