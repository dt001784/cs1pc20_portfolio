1. $ cd ~
// The folder "HOME" is opened
1. $ cd portfolio
// Opens the "portfolio" folder
2. $ git switch master
// Switches to the "master" branch.
3. $ mkdir -p week4/framework
// Creates a folder called "week4" and another folder inside called "framework"
4. $ cd week4/framework
// Opens the "week4" folder and then opens the "framework" folder
5. $ git branch framework
// Creates a new branch called "framework"
6. $ git switch framework
// Switches to the "framework" branch
7. $ nano Makefile:
feature:
<tab>mkdir $(NAME) ;\
<tab>cd $(NAME) && \
<tab>mkdir bin doc src test lib config ;\
<tab>echo “*” > bin/.gitignore ;\
<tab>echo “*” > lib/.gitignore
// Creates a Makefile which will be used to set up directories, libraries and gitignore files
8. $ cat -vTE Makefile
// Coppies the code from the Makefile with control characters to debug code
9. $ make feature NAME=test_output
// Sets "NAME" to the variable "test_output"
10. $ ls -al test_output
// Lists all folders and files
11. $ git add Makefile
// Adds all changes to the "Makefile" to the Git repository.
12. $ git commit -m "Setting up Makefile to create feature folders"
//  Commits the changes to the Git repository with the message "Setting up Makefile to create feature folders"
13. $ git push
// Pushes the committed changes to the Git repository.
14. $ cd test_output; cd src
// Opens the "src" folder inside of the "test_output" folder
15. Create a file called test_outputs.c with the following contents:
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define COM_SIZ 60
#define ARG_SIZ 1024
#define RES_SIZ 1024

int main(int argc, char *argv[]) {
    FILE *fp; /**< fp is a pointer used to give access to the file descriptor of the pipe */
    FILE *tests;
    char command[COM_SIZ];
    char arguments[ARG_SIZ];
    char expected[RES_SIZ];
    char actual[RES_SIZ];
    char command_line[COM_SIZ + ARG_SIZ];
    /** try to open the file named on the command line */
    tests = fopen(argv[1], "rt");
    if (tests == NULL) {
        printf("Error opening file %s\n", argv[1]);
        return 1;
    }
    /** we will read each line from the file.
     * These should be structured as:
     * command to run
     * inputs
     * expected output
     * Note: this could go horribly wrong if the input file is not
     * properly formatted
     */
    while (fgets(command, COM_SIZ, tests) != NULL) {
        fgets(arguments, ARG_SIZ, tests);
        fgets(expected, RES_SIZ, tests);
        /** string handling in C can be cumbersome.
         * typically suggestions online make use of "malloc" and
         * "strcpy" and "strcat"
         * but these complicate things and are arguably not good practice
         * strtok gives us a useful shortcut to
         * remove newlines (the way it is used here)
         */
        strtok(command, "\n");
        snprintf(command_line, sizeof command_line, "%s %s", command, arguments);
        /** Now we call the command, with the arguments and capture the result
         * so we can compare it to the expected result.
         * the "popen" command opens a special type of 'file' called a 'pipe'
         */
        fp = popen(command_line, "r");
        if (fp == NULL) {
            printf("Failed to run command\n");
            exit(1);
        }
        /** This is how we get the result back out of the pipe we opened
         * after reading the result into "actual" we compare it to the expected value
         * strcmp is slightly unusual - it returns
         * 0 if the strings are the same,
         * >0 if string1 is
         * bigger than string2, and
         * <0 if string1 is less than string2
         * because 0 is 'false', we negate (!) the result to test if
         * they are the same.
         */
        char message[RES_SIZ + RES_SIZ + 21];
        while (fgets(actual, sizeof(actual), fp) != NULL) {
            /** we create a message to let us know what was expected and what we got
             * note that we have split the line in the next statement – that is
             * fine, we can!
             */
            snprintf(message, sizeof message, "%s %s %s %s",
                "Expected ", expected, " and got ", actual);
            printf("%s", message);
            /** Because we want the test suite to keep running, we use an if
             * statement rather than the assert function
             */
            if (!strcmp(actual, expected)) {
                printf("OK\n");
            } else {
                printf("FAILED\n");
            }
        }
        /** if we don't close file handles, we risk using up the machine's resources
         */
        pclose(fp);
    }
    fclose(tests);
    return 0;
}
// Using "nano test_outputs.c" we code a c file that uses libraries to check if outputs match expected values and is used for testing
16. $ gcc -Wall -pedantic test_outputs.c -o test_outputs
// Compiles the "test_outputs.c" file into an executable file called "test_outputs" with the flags "-Wall" and "-pedantic"
17. $ ./test_outputs file_does_not_exist
// Attempts to run the program however the file does not exist and an error appears
18. $ ./test_outputs 
// Attempts to run the program however there is an error opening the file (null) {nothing is there}
19. Create a file called op_test with the following contents (note that there is no “newline” at the end of the
file):
wc -l
test_outputs.c
108 test_outputs.c
wc -l
test_outputs.c
82 test_outputs.c
wc -l
test_outputs.c
108 Test_outputs.c
// Using "nano op_test" we write the code and this compares the "Expected" output with the "Actual" output
20. $ ./test_outputs op_test
// This runs the compiled program with the op_test file. For the first comparison it was "OK", For the second comparison it "FAILED" (because the number of lines was wrong), For the third comparison it "FAILED" (due to the capital T in "108 Test_outputs.c").
22. $ git add op_test
// Adds all changes to the "op_test" to the Git repository.
23. $ git commit -m "test framework and sample test suite"
// Commits the changes to the Git repository with the message "test framework and sample test suite"
24. $ git push
// Pushes the committed changes to the Git repository.