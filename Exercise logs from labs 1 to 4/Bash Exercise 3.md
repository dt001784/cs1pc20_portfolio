1. $ cd ~
// The folder "HOME" is opened
2. $ cd portfolio
// Opens the "portfolio" folder
3. $ mkdir week3
// Creates a new directory called "week3"
4. $ mkdir week3/greeting
// Creates a folder inside week3 called "greeting"
5. $ cd week3/greeting
// Opens the greeting folder
6. $ git branch greeting
// Creates a new branch "greeting" 
7. $ git switch greeting
// Switches to the newly created "greeting" branch
8. Create a file called greeting.c with the following contents:
#include <stdio.h>
int greet(void) {
 printf("Hello world!\n");
 return 0;
}
// Using "nano greeting.c" the contents are written in the editor
9. $ gcc -Wall -pedantic -c greeting.c -o greeting.o
// Compiles "greeting.c" into an object called "greeting.o"
10. Create a file called test_result.c with the following contents:
#include <assert.h>
#include "greeting.h"
int main(void) {
 assert(0==greet());
 return 0;
}
// Using "nano test_result.c" the contents are written in the editor
11. And create a file called greeting.h with the following contents:
int greet(void);
// Using "nano greeting.h" the contents are written in the editor
12. $ echo greeting.o >> ~/portfolio/.gitignore
// Does not include "greeting.o" in the repository
13. $ echo libgreet.a >> ~/portfolio/.gitignore
// Does not include "libgreet.a" in the repository
14. $ ar rv libgreet.a greeting.o
// Creates a static library file called "libgreet.a" from "greeting.o"
15. $ gcc test_result.c -o test1 -L. -lgreet -I.
// Compiles the "test_result.c" into an executable file called "test1" and links it to a library called "libgreet.a" while including header files
16. $ ./test1
// Runs the "test1" executable which prints "Hello world!"
17. $ git add -A
Adds all changes to the Git repository.
18. $ git commit -m “greeting library and greeting test program”
// Commits the changes to the Git repository with the message "greeting library and greeting test program".
19. $ git push
// This does not work so we must use:
// "git push --set-upstream origin greeting" After logging in with csgitlab credentials this pushes the committed changes to the remote Git repository.
20. $ git switch master
// Switches to the "master" branch.
21. $ git branch vectors
// Creates a new branch called "vectors".
22. $ git switch vectors
// Switches to the newly created "vectors" branch.
23. $ cd ~/portfolio/week3
// Opens the "week3" folder inside of the "portfolio" folder
24. $ mkdir vectors
// Creates a new directory called "vectors" inside the "week3" directory.
25. $ cd vectors
// Opens the "vectors" directory
26. Create a file called vector.h with the following contents:
#define SIZ 3
int add_vectors(int x[], int y[], int z[]);
// Using "nano vecotr.h" opens the editior and contents are written into the editor
27. Create a file called test_vector_add.c with the following contents:
#include <assert.h>
#include "vector.h"
/** A simple test framework for vector library
* we will be improving on this later
*/
int main(void) {
/** xvec and yvec will be inputs to our vector arithmetic routines
* zvec will take the return value
*/
 int xvec[SIZ]={1,2,3};
 int yvec[SIZ]={5,0,2};
 int zvec[SIZ];
 add_vectors(xvec,yvec,zvec);
 /** We want to check each element of the returned vector
 */
 assert(6==zvec[0]);
 assert(2==zvec[1]);
 assert(5==zvec[2]);
 /** If the asserts worked, there wasn’t an error so return 0
 */
 return 0;
}
// Using "nano test_vector_add.c" opens the editior and contents are written into the editor
// The following code defines a chunk of memory for 3 integers in each array/vector
28. And now create the code to actually “do the math” – vector.c:
#include "vector.h"
/** A simple fixed size vector addition routine
* Add each element of x to corresponding element of y, storing answer in z
* It is the calling codes responsibility to ensure they are the right size
* and that they have been declared.
* We return an error code (0 in this case showing no error), but will add the
* program logic to handle actual errors later
*/
int add_vectors(int x[], int y[], int z[]) {
 for (int i=0;i<SIZ;i++)
 z[i]=x[i]+y[i];
 return 0;
}
// Using "nano vector.c" opens the editor and contents are written into the editor
29. $ gcc -Wall -pedantic -c vector.c -o vector.o
// Compiles the "vector.c" file into an object file called "vector.o" with the flags "-Wall" and "-pedantic"
30. $ ar rv libvector.a vector.o
// Archives the "vector.o" object file to the "libvector.a" library
31. $ gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I.
// Compiles the "test_vector_add.c" into an executable file called "test_vector_add1" and links it to a library called "libvector.a" while including header files
32. $ ./test_vector_add1
// Runs the "test_vector_add1" executable.
33. $ git add -A
// Adds all changes to the Git repository.
34. $ git commit -m “code to add two vectors of fixed size”
// Commits the changes to the Git repository with the message "code to add two vectors of fixed size".
35. $ git push
// Pushes the committed changes to the remote Git repository.
36. Change the assert(5==zvec[2]); line to be assert(5==zvec[1]); and recompile test to see what happens.
// Using "nano test_vector_add.c" to change the line in the editor
// "gcc test_vector_add.c -o test_vector_add1 -L. -lvector -I." recompiles the test program. However an error occurs that states "Assertation '5==z[1]' failed Aborted (core dumped)"
37. Edit vector.h so it contains this:
#define SIZ 3
int add_vectors(int x[], int y[], int z[]);
int dot_product(int x[], int y[], int z[]);
// Edits the "vector.h" file to add a function prototype for "dot_product".
38. Edit vector.c so it contains this:
#include "vector.h"
/** A simple fixed size vector addition routine
* Add each element of x to corresponding element of y, storing answer in z
* It is the calling codes responsibility to ensure they are the right size
* and that they have been declared.
* We return an error code (0 in this case showing no error), but will add the
* program logic to handle actual errors later
*/
int add_vectors(int x[], int y[], int z[]) {
 for (int i=0;i<SIZ;i++)
 z[i]=x[i]+y[i];
 return 0;
}
/** A simple fixed size dot product routine
* multiply each element of x to corresponding element of y, adding up totals
* It is the calling codes responsibility to ensure they are the right size
* and that they have been declared.
* We return the actual value we have calculated
* We may need program logic to handle actual errors later
*/
int dot_product(int x[], int y[]) {
 /** res <- a local variable to hold the result as we calculate it
 */
 int res = 0;
 for (int i=0;i<SIZ;i++)
 res=res + x[i]*y[i];
 return res;
}
// Use "nano vector.c" to add the lines in the editor
39. And create test_vector_dot_product.c so it contains:
#include <assert.h>
#include "vector.h"
/** A simple test framework for vector library
*/
int main(void) {
/** xvec and yvec will be inputs to our vector arithmetic routines
*/
 int xvec[SIZ]={1,2,3};
 int yvec[SIZ]={5,0,2};
 int result;
 result=dot_product(xvec,yvec);
 /** We want to check each element of the returned vector
 */
 assert(11==result);
return 0;
}
// Use "nano test_vector_dot_product.c" to add the lines in the editor
And then we compile to a library, and use that library when we compile the new test program:
1. $ gcc -Wall -pedantic -c vector.c -o vector.o
// Compiles the "vector.c" file into an object file called "vector.o" with the flags "-Wall" and "-pedantic"
2. $ ar rv libvector.a vector.o
// Archives the "vector.o" object file to the "libvector.a" library
3. $ gcc test_vector_dot_product.c -o test_vector_dot_product1 -L. -lvector -I.
// Compiles the "test_vector_dot_product.c" into an executable file called "test_vector_dot_product1" and links it to a library called "libvector.a" while including header files
4. $ ./test_vector_dot_product1
// Runs the "test_vector_dot_product1" executable.
5. $ git add -A
// Adds all changes to the Git repository.
6. $ git commit -m “code to calculate dot product of two vectors of fixed size”
// Commits the changes to the Git repository with the message "code to calculate dot product of two vectors of fixed size".
7. $ git push
// Pushes the committed changes to the remote Git repository.