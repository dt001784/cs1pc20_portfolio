1. $ mkdir -p $HOME/portfolio/week1 ; cd $HOME/portfolio/week1
// in the "HOME" folder a folder named "portifolio" is created and another folder inside "portifolio" named "week1" is created
2. $ cd ~
// the folder "HOME" is opened
3. $ rm -r portfolio
// the folder "portifolio" is deleted and everything inside it
4. $ mkdir -p $HOME/portfolio/week1 & cd $HOME/portfolio/week1
// in the "HOME" folder a folder named "portifolio" is created and another folder inside "portifolio" named "week1" is created AND opens the week1 folder
5. $ cd ~
// the home folder is opened
6. $ rm -r portfolio
// the folder "portifolio" is deleted and everything inside it
7. $ mkdir -p $HOME/portfolio/week1 && cd $HOME/portfolio/week1
// in the "HOME" folder a folder named "portifolio" is created and another folder inside "portifolio" named "week1" is created AND opens the week1 folder
// HOWEVER the command will only work if the first command is executed
8. $ echo "Hello World"
// "Hello World" is printed
9. $ echo Hello, World
// "Hello, World" is printed
10. $ echo Hello, world; Foo bar 11. $ echo Hello, world!
// "Hello, World" is printed and a message appears that says "Command 'Foo ' not found
12. $ echo "line one";echo "line two"
// "line one" and "line 2" and printed on seperate lines
13. $ echo "Hello, world > readme"
// prints "Hello, world > readme"
14. $ echo "Hello, world" > readme
// "Hello, world" is printed onto a file called readme
15. $ cat readme
// the file readme is opened and displayed
16. $ example="Hello, World"
// "Hello, World" is assigned to the variable 'example'
17. $ echo $example
// "Hello, world" is printed
18. $ echo ’$example’
// "$example" is printed
19. $ echo "$example"
// "Hello, world" is printed
20. $ echo "Please enter your name."; read example
// prints "Please enter your name." AND the input is stored in the variable 'example'
21. $ echo "Hello $example"
// prints "Hello" and also prints the value of the variable 'example'
22. $ three=1+1+1;echo $three
// the variable three is given the vlue "1+1+1" and prints "1+1+1"
23. $ bc (hint: check what the prompt says … also, “quit” is your friend)
// the bc calculator app is opened
24. $ echo 1+1+1 | bc
// prints the answer 3
25. $ let three=1+1+1;echo $three
// the variable 'three' is assigned to "1+1+1" and "1+1+1" is printed
26. $ echo date
// prints "date"
27. $ cal
// displays a calender in the current month and highlights the current date (based on your computer date and time settings)
28. $ which cal
// the file location of the cal operation is displayed
29. $ /bin/cal
// displays a calender in the current month and highlights the current date (based on your computer date and time settings)
30. $ $(which cal)
// displays a calender in the current month and highlights the current date (based on your computer date and time settings)
31. $ ‘which cal‘
// "which cal command not found" is displayed
32. $ echo "The date is $(date)"
// prints "the date is" AND prints the current date (based on your computer date and time settings)
33. $ seq 0 9
// prints a sequence of numbers from 0 to 9 on seperate lines
34. $ seq 0 9 | wc -l
// prints 10 BECAUSE it counts the number of lines used and prints it
35. $ seq 0 9 > sequence
// prints a sequence of numbers from 0 to 9 on seperate lines on to a file named 'sequence'
36. $ wc -l < sequence
// the number of lines in the 'sequence' file are printed
37. $ for I in $(seq 1 9) ; do echo $I ; done
// a for loop that prints the numbers 1 to 9 on seperate lines
38. $ (echo -n 0 ; for I in $(seq 1 9) ; do echo -n +$I ; done ; echo) | bc
// The sum of the sequence of numbers from 0 to 9, and the result is printed to the console (10 is printed)
39. $ echo -e ‘#include <stdio.h>\nint main(void) \n{\n printf(“Hello World\\n”);\n return 0;\n}’ > hello.c
// a file called hello.c is made and has code that prints "Hello World"
40. $ cat hello.c
// the file hello.c is opened and displayed (prints "Hello World")
41. $ gcc hello.c -o hello
// the code is compiled AND turned into an executable file
42. $ ./hello
// this runs the executable file AND prints "Hello World"