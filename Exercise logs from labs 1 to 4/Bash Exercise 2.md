$ cd ~ 
// the folder "HOME" is opened
$ git init portfolio 
// Initializes a new Git repository called "portfolio."
$ cd portfolio 
opens the "portfolio" folder
$ ls -al
//lists all folders/files inside of "portfolio"
$ git status
// checks the status of the repository (in this case there are "no commits yet")
$ echo hello > .gitignore
creats a file with the string "hello"
$ git add -A
// stages all changes
$ git status
//checks the status of the repository
$ git config --global user.email “<student_email>” 
// creates a global configuration for the student email
$ git config –global user.name “<student_name>” 
// creates a global configuration for the students name
$ git commit -m “first commit, adding week 1 content” 
// comits the staged changes
$ git status
// checks the status of the repository
$ git push
// error message appears "fatal no configured push destination" (destination is not specified in the push)
$ git remote add origin https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio.git
// sets the origin point for the file
$ git push –set-upstream origin master
// pushes the file to gitbub and then requires your username and password
$ git status 
// checks the status of the repository
$ echo “# CS1PC20 Portfolio” > readme.md
// Creates a readme file that says "# CS1PC20 Portfolio"
$ git add readme.md 
// stages the "readme.md" file 
$ git commit -m “added readme file”
// commits the "readme.md" file
$ git push
//pushes the file to the repository
$ git config –global credential.helper cache
// configures the global credential.helper variable for git
$ git branch week2
// creates a new branch "week2" 
$ git checkout week2 
// opens the "week2" branch
$ mkdir week2
// creates a new directory "week2"
$ echo “# Week 2 exercise observations” > week2/report.md
//creates a "report.md" file that is located in the "week2" folder
$ git status
//checks the status of the repository
$ git add week2
// stages the "week2" folder
$ git commit -m “added week 2 folder and report.md” 
// commits the additional changes in the week2 folder
$ git push
// pushes the modified week2 folder in the repository
$ git checkout master
// opens the master folder
$ ls -al
// lists all folders/files in the master folder
$ git checkout week2 
// opens the week2 folder
$ ls -al 
// lists all folders/files in the week2 folder
$ git checkout master
// opens the master folder
$ git merge week2
// merges the modifications from the week2 folder into the master folder
$ ls -al 
//lits all files/folders in the current folder
$ git push 
//pushes the modified files/folders into the repository
$ rm -r week2
//removes the week2 folder
$ rm -r week1 
//removes the week1 folder
$ ls -al 
// lists all files/folders (none left)
$ git status
//checks the status of the repository
$ git stash
// saves modifications temporarily 
$ git stash drop
// discards the stash
$ ls -al  
// lists all files/folders
$ cd ~ 
// the folder "HOME" is opened
$ cp -r portfolio portfolio_backup
// creates a backup of the portfolio folder
$ rm -rf portfolio 
removes the portfolio folder and all files inside
$ ls -al 
// lists all files/folders
$ git clone https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio portfolio 
// clones the repository in a portfolio folder
$ ls -al 
// lists all files/folders
